<html>
<head>
    <title>MINIM | Edit Lists</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
            <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="editPlaylists.css">
</head>
<script>
        var pid= "<?php echo  $_GET["playlistId"]; ?>";
        showSongs();
        function showSongs()
            {   
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                     if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("songContainer").innerHTML = this.responseText;
                        }
                      }
                xmlhttp.open("GET", "MINIM_Video.php?scope=editpage&pid="+pid,true);
                xmlhttp.send();
                            
                            
            }
         function deleteSong(sid,pid)
            {   
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                     if (this.readyState == 4 && this.status == 200) {
                                var res=this.responseText;
                                alert(res);
                        }
                      }
                      document.getElementById(sid).style.display="none";
                xmlhttp.open("GET", "MINIM_Video.php?scope=deletesong&sid="+sid+"&pid="+pid,true);
                xmlhttp.send();
                            
                            
            }   
            function deleteList(pid)
            {   
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                     if (this.readyState == 4 && this.status == 200) {
                                var res=this.responseText;
                                alert(res);
                                window.location.href="minim_video.php";
                                
                        }
                        
                      }
                xmlhttp.open("GET", "MINIM_Video.php?scope=deletelist&pid="+pid,true);
                xmlhttp.send();
                            
                            
            }   
            
            
</script>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" id="songContainer">
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </div>        
</body>
</html>
