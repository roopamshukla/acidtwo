<html>
<head>
    <title>MINIM</title>
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="js/seekbar.js"></script>
    <link rel="stylesheet" type="text/css" href="seekbar.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <link rel="icon" href="favicon.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="minim.css">
</head>
<script>/*<![CDATA[*/


        var user="<?php echo $_COOKIE["user"]; ?>";
        
        
        displayUser();
        recentlyPlayed();
        var i=-1; var la=0;
        no_rows=0;
        var nowPlaying=-1;
        getMostPlayed();
        getGenre();
        noOfPlaylists();
        var songid;
        currId=1; n=0;
        function recentlyPlayed()
        {
             var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("recentlyPlayed").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=keeprecent",true);
                        xmlhttp.send();
        }
                
        function setCookies(sid,sname)
        {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "MINIM_Main.php?scope=setcookie&sid="+sid+"&sname="+sname,true);
            xmlhttp.send();
            setTimeout(function(){
             var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("recentlyPlayed").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=recentplayed",true);
                        xmlhttp.send();},300);
        }
         function logout()
        {
        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                window.location.href=this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=logout",true);
                        xmlhttp.send();
            
        }    
        function displayUser()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("userHead").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=user&uid="+user,true);
                        xmlhttp.send();
                        
                        
        }
        function searchBlur()
        {
            document.getElementById("searchBar").style.borderBottomColor="#ccc";
        }
        function searchFocus()
        {
            document.getElementById("searchBar").style.borderBottomColor="#66afe9";
        }    
        function unhide()
        {
            document.getElementById("searchDiv").style.display="block";
            searchBlur();
        }
        function hide()
        {
            document.getElementById("searchDiv").style.display="none";
                      
        }
        function pause()
        {
            document.getElementById("songAudio").pause();
            document.getElementById('playPause').setAttribute("src","controls/play.png");
            document.getElementById('playPause').setAttribute("onclick","play()");
            return;
            
        }
         function formatTime(seconds) {
            minutes = Math.floor(seconds / 60);
            minutes = (minutes >= 10) ? minutes : "0" + minutes;
            seconds = Math.floor(seconds % 60);
            seconds = (seconds >= 10) ? seconds : "0" + seconds;
            return minutes + ":" + seconds;
          }
        function changeSeek()
        {
            var song =document.getElementById("songAudio");
            song.currentTime=seekbar.value;
        }
        function play()
        {
            document.getElementById("songAudio").play();
            document.getElementById('playPause').setAttribute("src","controls/pause.png");
            document.getElementById('playPause').setAttribute("onclick","pause()");
            var song =document.getElementById("songAudio");     
              song.ontimeupdate = function() {myFunction()};
              function myFunction() {
                    document.getElementById("startTime").innerHTML = formatTime(song.currentTime);
                    var maxTime=parseInt(document.getElementById("songAudio").duration);
                    document.getElementById("endTime").innerHTML =formatTime(maxTime);
                    
                    seekbar.setValue(song.currentTime);
                    seekbar.maxValue=parseInt(maxTime);
                }
            return;
            
        }
        function setArtist(sid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("artistName").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=artist&sid="+sid,true);
                        xmlhttp.send();
                        
                        
        }
        function setAlbum(sid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("albumName").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=setalbum&sid="+sid,true);
                        xmlhttp.send();
                        
                        
        }
        function songYear(sid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("yearOfRelease").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=year&sid="+sid,true);
                        xmlhttp.send();
                        
                        
        }
        var last=0; var c=0;
        colors =["violet","#32cd32","#F06292","#1E88E5","#b7b700","#ffa500","#AD1457","#0097A7","#558B2F","#607D8B"];
        function currentPlay(sid,sname,q)
        {
            currId = sid;
            
            if(c==9)
            {
                c=0;
            }
            if(q==-1)
            {
                nowPlaying=-1;
                document.getElementById("comingUp").innerHTML="<a href='#' class='list-group-item'><i>Single play</i></a>";
            }
            document.getElementById('searchBar').style.borderBottomColor=colors[c];
            c++;
            document.getElementById('albumArt').setAttribute("src","albumart/"+sid+".jpg");
            setArtist(sid);
            setAlbum(sid);
            songYear(sid);
           
            document.getElementById('songAudio').setAttribute("src","songbase/"+sid+".mp3");
            
            setCookies(sid,sname);
            
            play();
            document.getElementById('songName').innerHTML=sname;
            
            if(nowPlaying!=-1)
            {
                document.getElementById('nextSong'+last).style.background="#fff";
                document.getElementById('nextSong'+last).style.color="#555";
                }
            if(q!=-1)
            {    
                document.getElementById('nextSong'+q).style.background="#FF6D00";
                document.getElementById('nextSong'+q).style.color="#fff";
              }  
                    last=q;
                
                nowPlaying=q;
            
            
            document.getElementById("songAudio").addEventListener("ended",nextSong,false);
            hide();
            return ;
        }
        function prevSong()
        {
            
            if(nowPlaying==0)
            {
                return;
            }
            if(nowPlaying==-1)
            {
                return;
            }
            nowPlaying--;
            document.getElementById("nextSong"+nowPlaying).click();
        }
        function nextSong()
        {
            if(nowPlaying==no_rows)
            {
                return;
            }
            if(nowPlaying==-1)
            {
                return;
            }
            
            nowPlaying++;
            document.getElementById("nextSong"+nowPlaying).click();
        }
        
        function volume(str)
        {
           
            document.getElementById("songAudio").volume=(parseInt(str)*(0.1)).toFixed(1);
            return;
            
        }
        
        function volumeShow()
        {
            document.getElementById("volume-icon").style.display="none";
            document.getElementById('volume').setAttribute("type","range");
        }
        function volumeHide(str)
        {
            if(str==0)
            {
                document.getElementById('volume-icon').setAttribute("src","controls/mute.png");
                setTimeout(function(){
                document.getElementById("volume-icon").style.display="initial";
                document.getElementById('volume').setAttribute("type","hidden");
                },790);
            }
            else
            {
                document.getElementById('volume-icon').setAttribute("src","controls/volume.png");
                setTimeout(function(){
                document.getElementById("volume-icon").style.display="initial";
                document.getElementById('volume').setAttribute("type","hidden");
                },790);
            }
            return;
            
        }
        function albumShow(aid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("comingUp").innerHTML = this.responseText;
                                 nowPlaying=-1;
                              
                              no_rows = document.getElementById('norows5').value;
                              pause();
                            document.getElementById("nextSong"+0).click(); 
                            }
                            
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=album&key="+aid,true);
                        xmlhttp.send();
                        document.getElementById("comingUp").style.display="block";
                        
        }
        function getMostPlayed()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("mostPlayed").innerHTML = this.responseText;
                                no_rows = document.getElementById('norows2').value;

                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=mostplay",true);
                        xmlhttp.send();
                        
                        
        }
        function getGenre()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("genreList").innerHTML = this.responseText;
                              }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=genre",true);
                        xmlhttp.send();
                        
                        
        }
        function playGenre(gid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("comingUp").innerHTML = this.responseText;
                                 nowPlaying=-1;
                              no_rows = document.getElementById('norows6').value;
                              pause();
                              if(no_rows!=-1)
                              {
                            	document.getElementById("nextSong"+0).click();
                            } 
                            }
                            
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=playgenre&gid="+gid,true);
                        xmlhttp.send();
                        document.getElementById("comingUp").style.display="block";
                        
        }
        var flag=0;
        var songid;
        function mostPlayedList(sid,sname)
        {   
            
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("comingUp").innerHTML = this.responseText;
                                no_rows = document.getElementById('norows1').value;
                                songid=JSON.parse(document.getElementById('jsonSpan').innerHTML);
                                
                             for(m=0;m<=no_rows;m++)
                             {
                                if(sname== (songid[m].replace(/&amp;/g, "&")) )
                                {
                                    
                                    flag=1;
                                    break;
                                }
                             }
                             if(flag==1)
                             {
                                pause();
                                document.getElementById("nextSong"+m).click();
                             }
                             else
                             {
                                currentPlay(sid,sname,"-1");
                             }
                            }
                           
                             
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=mostplaylist",true);
                        xmlhttp.send();
                        document.getElementById("comingUp").style.display="block";
                        
                        
                        
        }
        function noOfPlaylists()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("playlistBadge").innerHTML = this.responseText;
                            }
                            
                                
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=noplaylist&user="+user,true);
                        xmlhttp.send();
                        
                        
        }
        var show=0;
        function showPlaylists()
        {   
            if(show===0)
            {
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("playlistContainer").innerHTML = this.responseText;
                                show=1;
                            }  
                            document.getElementById("playlistContainer").style.display="block";
                            document.getElementById("playlistBadge").style.display="none";  
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=playlist&user="+user,true);
                        xmlhttp.send();
             }
             else    
             {
                    document.getElementById("playlistContainer").style.display="none";
                    document.getElementById("playlistBadge").style.display="initial";  
                    show=0;
             }       
                        
        }
        function playedList(pid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("comingUp").innerHTML = this.responseText;
                                
                              nowPlaying=-1;
                              no_rows = document.getElementById('norows3').value;
                              pause();
                              document.getElementById("nextSong"+0).click(); 
                              
                            }
                                
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=playplaylist&key="+pid,true);
                        xmlhttp.send();
                        document.getElementById("comingUp").style.display="block";
                        
        }
        function addToPlaylist()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("addToLists").innerHTML = this.responseText;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=addsong&user="+user,true);
                        xmlhttp.send();
                        
                        
        }
        function addNewPlaylist()
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                               var res=this.responseText;
                                alert(res);
                                noOfPlaylists();
                                showPlaylists();
                            }
                        }
                        document.getElementById("modal-go").click();
                        var lname = document.getElementById("createList").value;
                        document.getElementById("createList").value="";
                        xmlhttp.open("GET", "MINIM_Main.php?scope=addnewlist&sid="+currId+"&lname="+lname+"&user="+user,true);
                        xmlhttp.send();
                        
                        
        }
        function addToExisting(pid)
        {   
            var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                               var res=this.responseText;
                                alert(res);
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=addtolist&sid="+currId+"&pid="+pid+"&user="+user,true);
                        xmlhttp.send();
                        
                        
        }
        function showEdit(l)
        {
            document.getElementById("editPlaylist"+l).style.visibility="visible";
        }
        function hideEdit(l)
        {
            document.getElementById("editPlaylist"+l).style.visibility="hidden";
        }
        function redirectEdit(pid)
        {
            window.location.href="/Minim/editplaylists.minim.php?playlistId="+pid;
        }
        function search_func(str) {
        
                    if(event.keyCode == 27)
                       {
                       document.getElementById("searchBar").value = "";
                       document.getElementById("searchBar").style.borderBottomColor="#66afe9";
                       hide();
                       return;
                       } 
        
                    else if (str.length== 0 || !str || str==" ") { 
                        i=-1;
                        document.getElementById("searchBar").value = "";
                        document.getElementById("searchBar").style.borderBottomColor="#66afe9";
                        document.getElementById("searchBar").setAttribute("placeholder","Search Minim");
                        hide();
                        return;
                    } else {
                                            
                        if(event.keyCode == 38 && document.getElementById('searchResult').getAttribute('data-value').localeCompare("1")==0)
                        {
                            return false;
                        }
                        else if(event.keyCode == 38 && document.getElementById('searchResult').getAttribute('data-value').localeCompare("2")==0)
                        {
                            i--;
                            if(i<-1)
                            {   
                                
                                i++;
                                
                                return;
                            }
                            if(i==-1)
                            {   
                                
                                document.getElementsByClassName(i+1)[0].style.background="white";
                                return;
                            }
                            else if(i==0)
                            {
                                 document.getElementsByClassName(i)[0].style.background="#e0e0e0";
                                 
                                 document.getElementsByClassName(i+1)[0].style.background="white";
                                 
                                 
                            }
                            else if(i>0)
                            {
                                document.getElementsByClassName(i+1)[0].style.background="white";
                                 document.getElementsByClassName(i)[0].style.background="#e0e0e0";
                                 
                                 
                                 
                            }

                            
                        }
                        else if(event.keyCode == 40 && document.getElementById('searchResult').getAttribute('data-value').localeCompare("1")==0)
                        {
                            return false;
                        }
                        else if(event.keyCode == 40 && document.getElementById('searchResult').getAttribute('data-value').localeCompare("2")==0)
                        {       
                                no_rows = document.getElementById('norows').value;

                                i++;
                                if(i>=no_rows)
                                {   
                                    i--;
                                    return;
                                }
                                else if(i==0)
                                {
                                    
                                    document.getElementsByClassName(i)[0].style.background="#e0e0e0";
                                    
                                    
                                }
                                else
                                {
                                    document.getElementsByClassName(i)[0].style.background="#e0e0e0";
                                    
                                    document.getElementsByClassName(i-1)[0].style.background="white";
                                    
                                }

                                
                        }
                        else
                        {
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("searchDiv").innerHTML = this.responseText;
                               no_rows = document.getElementById('norows').value;
                            }
                        }
                        xmlhttp.open("GET", "MINIM_Main.php?scope=search&key="+str,true);
                        xmlhttp.send();
                        unhide();
                        
                        if(i>=0)
                        {
                            if(event.keyCode == 13)
                                 {
                                    
                                   pause();
                                   document.getElementById("link"+i).click();
                                   

                                   
                                 } 
                                 
                        }
                        if(no_rows==1 && event.keyCode == 13)
                                {
                                    
                                    pause();
                                    document.getElementById("link"+0).click();
                                    
                                    
                                }
                        
                       
                       i=-1;
                        
                    }
                   
                }
                
                 
                }
                        document.addEventListener("click",hide);     
                                   
                
            function resizeSeek()
            {

                    document.getElementsByClassName("seekbar-positive")[0].style.width="99%";
            }
                             
   /*]]>*/</script>
<body onresize="resizeSeek()">

            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <div class="navbar-header">
                  <a class="navbar-brand"><img src="logo.jpg" style="width:110px;margin-left:10px;margin-top:-4px;"/></a>
                   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span> 
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
				  <li class="dropdown" >
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown"  id="playlistHead" onclick="showPlaylists()" return false;>Playlists  <span class="badge" id="playlistBadge"></span><span class="caret"></span></a>
					<ul class="dropdown-menu" id="playlistContainer" style="width:150%;">
					  <li><a href="#">Page 1-1</a></li>
					  <li><a href="#">Page 1-2</a></li>
					  <li><a href="#">Page 1-3</a></li> 
					</ul>
				  </li>
				</ul>
                
                  <ul class="nav navbar-nav navbar-right">
              	  <li><a id="songPage" style="background:#333;color:white;" href="#">Songs</a></li>
              	  <li id="videoPage"><a href="minim_video.php">Videos</a></li> 
                  <li class="dropdown active" >
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="userHead"></a>
					<ul class="dropdown-menu" style="width:150%;">
					  <li><a href="#" onclick="logout()" style="font-size:1.1em;font-family:'Varela Round', sans-serif;">Logout</a></li>
					</ul>
				  </li>
                  </ul>
                </div>
              </div>
            </nav>
        
    
            <div class="col-md-2">
            <!--profile and logo-->
               <a href="#" onclick="getGenre()" class='list-group-item' id="genre-head">Genres<i class="material-icons" style="vertical-align:middle;padding:0px 0px 0px 5px">featured_play_list</i></a>
                    <div class="list-group" id="genreList">
                    <!--glists-->
                    </div>
                </div>
            <div class="col-md-7" >
            <!--main interface-->
                <div class="row" >
                    <!--search bar-->
                    <form>
                    <input type="text" placeholder="Search Minim" onkeypress="return event.keyCode != 13" class="form-control" id="searchBar" onkeyup="search_func(this.value)" autocomplete="off" onfocus="searchFocus()" onblur="searchBlur()" autofocus>
                    </form>
                    <div id="searchDiv">
                    <!--search-appears-here-->
                    </div>
                </div>
                <div class="row table-responsive">
                    <div>
                     <!--song art-->
                     <table id="sinfo-table">
                         <tr>
                            <td rowspan="4" id="albumArt-cell">
                                    <img src="albumart/1.jpg" id="albumArt" >
                            </td>
                            <td id="songName">Hymn For The Weekend</td>
                            <td rowspan="4" style="vertical-align:top;text-align:center;padding-top:10px;">
                                
                                 <div class="dropdown" >
                                     <a href="#" onclick="addToPlaylist()" data-toggle="dropdown" id="plusPlaylist"><i class="material-icons addSong">add</i></a>
                                     
                                      <ul class="dropdown-menu" id="addToLists">
                                        <li class="menu-item"><a href="#">List 1</a></li>
                                        <li class="menu-item"><a href="#">List 2</a></li>
                                        <li class="menu-item"><a href="#">List 3</a></li>
                                      </ul>
                                      
                                    </div>
                                    
                            </td>
                         </tr>
                         <tr>
                            
                            <td id="artistName"> Artists : Coldplay<br>
                            <small></small> 
                            </td>
                         </tr>
                         <tr>
                            
                            <td id="albumName">Album : A Head Full Of Dreams
                            </td>
                         </tr>
                         <tr>
                            
                            <td id="yearOfRelease">Year : 2015
                            </td>
                            
                         </tr>
                     </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1" id="startTime">00:00</div>
                   
                    <div id="seekbar-container" class="col-md-10" draggable="true" onclick="changeSeek()">
                    </div>
                    <div class="col-md-1" id="endTime">00:00</div>
                    <script type="text/javascript">

                        var seekbar = new Seekbar.Seekbar({
                            
                            renderTo: "#seekbar-container",
                            minValue: 1, maxValue: 255,
                            valueListener: function (value) {
                                this.setValue(Math.round(value));

                            }
                        });
                    </script>
                </div>
                
                
                <div class="row">
                    <div >
                     <!--control buttons-->
                        <audio controls style="display:none;" id="songAudio">
                          <source src="songbase/1.mp3" type="audio/mpeg">
                           Your browser does not support the audio element.
                        </audio>
                        <table id="controlTable" width="100%">
                            <tr>
                                <td id="prev-btn"><img src="controls/previous.png" onclick="prevSong()" width="40px" height="40px" ></td>
                                <td id="play-btn"><img src="controls/play.png" onclick="play()" width="40px" height="40px" id="playPause"></td>
                                <td id="next-btn"><img src="controls/next.png" onclick="nextSong()" width="40px" height="40px"></td>
                                <td id="volume-btn"><img src="controls/volume.png" id="volume-icon" width="30px" height="30px" onclick="volumeShow()"><input id="volume" value="7" type="hidden" min="0" max="10" oninput="volume(this.value)" onmouseup="volumeHide(this.value)"/></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                <a href="#" onclick="recentlyPlayed()" class='list-group-item' id="comingUp-head">Coming Up<i class="material-icons" style="vertical-align:middle;padding:0px 0px 0px 5px">playlist_play</i></a>
                    <div class="list-group" id="comingUp">
                     <!--next playing list-->    
                     <a href='#' class='list-group-item'><i>No playlist selected</i></a>  
                    </div>
                </div>
            </div>
            <div class="col-md-3">
            <!--lists-->
                <div class="row">
                <a href="#" onclick="getMostPlayed()" class='list-group-item' id="mostPlayed-head">Most Played<i class="material-icons" id="whatshot" style="vertical-align:middle;margin-top:-5px;margin-left:4px;">whatshot</i></a>
                    <div class="list-group" id="mostPlayed">
                    <!--mlists-->
                    </div>
                </div>
                <div class="row">
                    <a href="#" onclick="recentlyPlayed()" class='list-group-item' id="recentlyPlayed-head">Recently Played</a>
                    <div class="list-group" id="recentlyPlayed">
                    <!--rlists-->
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                   <div class="modal-content">
                    <div class="modal-body">
                       <form id="createList-form" >
                                        <input type="text" id="createList" placeholder="Playlist Name" autocomplete='off' onkeypress="if(event.keyCode == 13){addNewPlaylist();return event.keyCode != 13}" name="uname"></input>
                                        <a class="material-icons" data-dismiss="modal" style="vertical-align:middle;cursor:pointer;text-decoration:none;color:#777" id="modal-go">clear</a><br>
                                        <input class="btn btn-success playList-btn" onclick="addNewPlaylist()" type="button" style="white-space:normal;" id="" value="Create"/>
                                    <form>
                    </div>
                  </div>
                  
                </div>
              </div>
              
    </div>

    <p id="test"></p>
</body>

</html>
